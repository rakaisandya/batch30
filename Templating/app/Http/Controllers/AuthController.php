<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('halaman.register');
    }
    public function welcome(Request $request){
       // dd($request->all());
       $depan = $request['namadepan'];
       $belakang = $request['namabelakang'];

       return view('halaman.welcome', compact('depan','belakang')); 
    }
}
