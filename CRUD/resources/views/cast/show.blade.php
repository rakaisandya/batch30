@extends('layout.master')
@section('judul')
    Detail Cast
@endsection
@section('content')
    
<h2>Cast Number {{$cast->id}}</h2>
<h3>Nama</h3>
<p>{{$cast->nama}}</p>
<h3>Umur</h3>
<p>{{$cast->umur}}</p>
<h4>Bio </h4>
<p>{{$cast->bio}}</p>
@endsection