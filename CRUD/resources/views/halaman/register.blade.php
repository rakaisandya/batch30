@extends('layout.master')
@section('judul')
Buat Account Baru!
@endsection
@section('content')
    
<h2>
    Sign Up Form
</h2>
<form action="/welcome" method="post">
@csrf
<label>
    First name:<br><br>
</label>       
<input type="text" placeholder="Nama Depan Anda" name="namadepan"><br><br>
<label>
    Last name:<br><br>
</label>
    <input type="text" placeholder="Nama Belakang Anda" name="namabelakang"><br><br>
<label>
    Gender:<br><br>
</label>
    <input type="radio" name="gender" value="0"> Male<br>
    <input type="radio" name="gender" value="1"> Female<br>
    <input type="radio" name="gender" value="2"> Other <br><br>
<label>
    Nationality:<br><br>
</label>
<select>
    <option value="indo">Indonesian</option>
    <option value="malay">Malaysian</option>
    <option value="singapur">Singaporean</option>
    <option value="aussie">Australian</option>
</select>
<br><br>
<label>
    Language Spoken:<br><br>
</label>
    <input type="checkbox" name="bahasa" value="0"> Bahasa Indonesia<br>
    <input type="checkbox" name="bahasa" value="1"> English<br>
    <input type="checkbox" name="bahasa" value="2"> Other<br><br>
<label>
    Bio:<br><br>
</label>
    <textarea name="bio" cols="40" rows="10" placeholder="Your bio goes here..."></textarea><br>
    <br>
<input type="submit" value="Sign Up">
</form>
@endsection
