<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('W3D2.register');
    }
    public function welcome(Request $request){
       // dd($request->all());
       $depan = $request['namadepan'];
       $belakang = $request['namabelakang'];

       return view('W3D2.welcome', compact('depan','belakang')); 
    }
}
